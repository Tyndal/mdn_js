// let myHeading = document.querySelector('h1');
// myHeading.textContent = 'Hello World!';

let myImage = document.querySelector('img');

myImage.onclick = function() {
  let mySrc = myImage.getAttribute('src');

  if (mySrc === 'images/firefox-icon.png') {
    myImage.setAttribute('src', 'images/react.png');
  } else {
    myImage.setAttribute('src', 'images/firefox-icon.png');
  }
};

let userButton = document.getElementById('changeUser');
let myHeading = document.querySelector('h1');

function setUserName() {
  let myName = prompt('Please, enter Your name');
  localStorage.setItem('name', myName);
  myHeading.textContent = 'Mozilla is cool, ' + myName;
}

if (!localStorage.getItem('name')) {
  setUserName();
} else {
  let storedName = localStorage.getItem('name');
  myHeading.textContent = 'Mozilla is cool, ' + storedName;
}

userButton.onclick = function() {
  setUserName();
};

// Creating paragraph onclick

function createParagraph() {
  let para = document.createElement('p');
  para.textContent = 'You clicked the button!';
  document.body.appendChild(para);
}

let buttons = document.getElementsByClassName('createParagraphBtn');

for (let i = 0; i < buttons.length; i++) {
  buttons[i].addEventListener('click', createParagraph);
}

//////////////////////////////////////////////////////////////////

let enterName = document.getElementById('enterName');

function enterYourName() {
  let typedName = prompt('Type Your name, pls!');
  alert (`Hello ${typedName}`);
};

enterName.addEventListener('click', enterYourName);
